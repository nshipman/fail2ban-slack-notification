#!/usr/bin/env python 
import os  
import socket
import sys 
import argparse 
from slackclient import SlackClient 

def parse_args(): 
    parser = argparse.ArgumentParser(add_help=True) 
    parser.add_argument(
        '--port','-p', 
        action = 'store', 
        type = str, 
        help = "Port number"
    )
    parser.add_argument(
        '--ip', 
        action = 'store', 
        type = str, 
        help = "Ip address for banned IP"
    ) 
    parser.add_argument(
        '--jail','-j',
        action = 'store', 
        type = str, 
        help = "Name of the Jail"
    ) 
    parser.add_argument(
        '--failures','-f',
        action = 'store',  
        type = str, 
        help = "Number of failures for the jail"
    )
    parser.add_argument(
        '--ban','-b',
        action = 'store_true',  
        help = "If flagged will call the banned message"
    )   

    parser.add_argument(
        '--unban','-u', 
        action = 'store_true', 
        help = "If flagged will call the unban message"
    )
    return parser.parse_args() 

def main(): 
    slack_token = os.environ["SLACK_BOT_TOKEN"] # Pull the Slack token from your OS environment variable
    slack_channel = "" # Define the channel you would like to send your notifications to.
    slack_bot_name = "" # Define the name of your slackbot
    hostname = socket.getfqdn()
    sc = SlackClient(slack_token)  
    args = parse_args() 
    # Variables taken from fail2ban slack.conf 
    ip_address = args.ip 
    jail = args.jail
    failures = args.failures
    sc.api_call(
        "channels.join",
        channel = slack_channel,
        vaildate = True
    )
    if args.ban:
        sc.api_call(
        "chat.postMessage",
        channel = slack_channel, 
        text = "{} - Ban Alert: {} banned After {} failures in the jail {}".format(hostname,ip_address,failures,jail),
        username = slack_bot_name
        ) 
    
    elif args.unban: 
        sc.api_call(
        "chat.postMessage",
        channel = slack_channel, 
        text = "{} - Unban Notification: {} has been unbanned from the jail {}".format(hostname,ip_address,jail),
        username = slack_bot_name
        )  
    
    else: 
        print("Notification type not defined. No Message Sent")

if __name__ == "__main__":
    main()