# Fail2ban Slack Notifications 

Decided to create a simple python extension to forward messages to slack because: 

* When originally installing fail2ban version 0.8 on Ubuntu 14.04, the mail settings did not work. 
* Other team members did not want to get their email blasted by fail2ban notificaitons. 

## Requirements 

* Python3.6 or later 
* slackclient package 
* fail2ban 

## Additional Notes 

The configuration files for the fail2ban service were tested and built for the latest version of fail2ban 0.10.2-2 on Ubuntu 18.04. 
The biggest issue would be the how systemd handles environment variables. Systemd requires an environment variable file to load the values. 
init.d is capable of using environment variables pulled from the user. Since Ubuntu 14.04 is deprecated, configurations files will be for 
systemd. 

## Upcoming 

Adding an ansible fail2ban role to install the setup.